const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex );
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16); // "02"
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex); // ff0000
    },

    hexToRGB: (hex) => {
        const redRGB = parseInt(hex.slice(0,2), 16)//.toString()
        const greenRGB = parseInt(hex.slice(2,4),16)//.toString()
        const blueRGB = parseInt(hex.slice(4,6),16)//.toString()

        return `(${redRGB}, ${greenRGB}, ${blueRGB})`
    }
}
